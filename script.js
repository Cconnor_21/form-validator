//TODO: Add Jest Testing

const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const password2 = document.getElementById('password2');

//show password icon elements
const pass = document.getElementById('pass');
const pass2 = document.getElementById('pass2');

//button element
const regButton = document.getElementById('btn');


//show input error Message
function showError(input, message){
  const formControl = input.parentElement;
  formControl.className = 'form-control error';
  const small = formControl.querySelector('small');
  small.innerText = message;
  errorButton();
}

//show and hide password
function togglePassword(elem){
  if(elem.className === "fa fa-eye-slash pass"){
    //changes input type from text to password to show password
    elem.parentElement.children[2].type = "text";
    //changes color of show password icon to darker color
    elem.style.color = '#909090';
    //changes font awesome icon to different icon
    elem.className = "fa fa-eye pass";
  }
  else{
    //changes input type from password to text to hide password
    elem.parentElement.children[2].type = "password";
    //changes color of show password icon back to lighter color
    elem.style.color = '#C1C1C1';
    //changes font awesome icon back to original hide password icon
    elem.className = "fa fa-eye-slash pass";
  }
}

//show success outline
function showSuccess(input){
  const formControl = input.parentElement;
  formControl.className = 'form-control success';
}

//capatilize first letter of String
function firstToUpper(str){
  var out = str.charAt(0).toUpperCase() + str.slice(1);
  return out;
}

//check if password contains a number and a special character like $, &, or #
function checkForSpecial(input){
  const special = /\W/g;
  const num = /\d/;
  console.log(special.test(input.value) == true && num.test(input.value) == true);
  if(special.test(input.value) == true)
  {
    showSuccess(input);
  }
  else if (special.test(input.value) == false){
    showError(input, 'One number and special character is required');
  }
}

//check passwords match
function checkPasswordsMatch(input1, input2){
  if(input1.value !== input2.value){
    showError(input2, 'Passwords do not match');
  }
}

//Check email is valid
function checkEmail(input){
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if(re.test(input.value)){
    showSuccess(input);
  }
  else{
    showError(input, 'Email is not valid');
  }
}

//check required fields
function checkRequired(inputArr){
  inputArr.forEach(function(input) {
    if(input.value.trim() === ''){
      showError(input, `${firstToUpper(input.id)} is required`);
    }
    else{
      showSuccess(input);
    }
  });
}

//check input length
function checkLength(input, min, max){
  if(input.value.length < min){
    showError(input, `${firstToUpper(input.id)} must be at least ${min} characters`);
  }
  else if(input.value.length > max){
    showError(input, `${firstToUpper(input.id)} cannot be more than ${max} characters`);
  }
}

//Animate button if any field doesn't pass validation
function errorButton(){
  console.log('shake');
  regButton.className = "animated shake";
}


//event listeners
pass.addEventListener('click', function(){
  togglePassword(this);
});

pass2.addEventListener('click', function(){
  togglePassword(this);
});

form.addEventListener('submit', function(e){
  e.preventDefault();

  checkRequired([username, email, password, password2]);
  checkLength(username, 3, 15);
  checkLength(password, 6, 25);
  checkEmail(email);
  checkForSpecial(password);
  checkPasswordsMatch(password, password2);
});
